<?php

/**
 * Implements hook_monolog_handler_info().
 */
function monolog_slack_monolog_handler_info() {
  $handlers = array();

  $handlers['slack'] = array(
    'label' => t('Slack Handler'),
    'description' => t('Logs records into any Slack stream.'),
    'handler file' => drupal_get_path('module', 'monolog_slack') . '/handlers/slack.inc',
    'group' => t('Alerts and emails'),
    'default settings' => array(
      'token' => '',
      'channel' => '',
      'username' => 'Monolog',
      'icon_emoji' => 'japanese_ogre',
      'use_attachment' => TRUE,
      'use_short_attachment' => TRUE,
      'include_context_and_extra' => FALSE,
    ),
  );

  return $handlers;
}
