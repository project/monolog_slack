<?php

/**
 * @file
 * Handler include for HipChatHandler.
 */

use Monolog\Handler\SlackHandler;

/**
 * Monolog loader callback; Loads a Slack handler.
 *
 * @return HandlerInterface
 */
function monolog_slack_handler_loader($options) {
  return new SlackHandler($options['token'], $options['channel'], $options['username'], $options['use_attachment'], $options['icon_emoji'], $options['level'], $options['bubble'], $options['use_short_attachment'], $options['include_context_and_extra']);
}

/**
 * Monolog settings form; Settings for the HipChat handler.
 */
function monolog_slack_handler_settings(&$form, &$form_state, $profile, array $handler) {

  $form['token'] = array(
    '#title' => t('Slack API Token'),
    '#type' => 'textfield',
    '#default_value' => $handler['token'],
    '#description' => t('Slack API Token.'),
    '#required' => TRUE,
  );

  $form['channel'] = array(
    '#title' => t('Channel'),
    '#type' => 'textfield',
    '#default_value' => $handler['channel'],
    '#description' => t('The room that should be alerted of the message (Id or Name).'),
    '#required' => TRUE,
  );

  $form['username'] = array(
    '#title' => t('Username'),
    '#type' => 'textfield',
    '#default_value' => $handler['username'],
    '#description' => t('Name used in the "from" field.'),
    '#required' => TRUE,
  );

  $form['icon_emoji'] = array(
    '#title' => t('Emoji'),
    '#type' => 'textfield',
    '#default_value' => $handler['icon_emoji'],
    '#description' => t('Icon used in the message.'),
    '#required' => TRUE,
  );

  $form['use_attachment'] = array(
    '#title' => t('Use attachment for body.'),
    '#type' => 'checkbox',
    '#default_value' => $handler['use_attachment'],
    '#description' => t('Use attachments.'),
  );

  $form['use_short_attachment'] = array(
    '#title' => t('Use short attachments.'),
    '#type' => 'checkbox',
    '#default_value' => $handler['use_short_attachment'],
    '#description' => t('Use short attachments.'),
  );

  $form['include_context_and_extra'] = array(
    '#title' => t('Include context.'),
    '#type' => 'checkbox',
    '#default_value' => $handler['include_context_and_extra'],
    '#description' => t('Inlude context and extra.'),
  );
}
